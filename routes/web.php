<?php

Route::get('pagina.inicial.sistema', 'EstoqueController@EstoquePaginaInicialSistema')->name('pagina.inicial.sistema');

/*Produtos*/
Route::get('pagina.lista.produtos', 'EstoqueController@EstoquePaginaListaProduto')->name('pagina.lista.produtos');
Route::get('form.produto', 'EstoqueController@EstoqueCadastraProduto')->name('form.produto');
Route::post('salvar.produto', 'EstoqueController@EstoqueSalvarProduto')->name('salvar.produto');
Route::get('editar.produto/{id}', 'EstoqueController@EstoqueEditarProduto')->name('editar.produto');
Route::post('salvar.produto.editado/{id}', 'EstoqueController@EstoqueSalvarProdutoEditado')->name('salvar.produto.editado');
Route::delete('produto.deletar/{id}', 'EstoqueController@EstoqueDeletaProduto')->name('produto.deletar');


/*Vereadores */

Route::get('pagina.lista.vereadores', 'EstoqueController@EstoquePaginaListaVereadores')->name('pagina.lista.vereadores');
Route::get('form.vereador', 'EstoqueController@EstoqueCadastraVereador')->name('form.vereador');
Route::post('salvar.vereador', 'EstoqueController@EstoqueSalvarVereador')->name('salvar.vereador');
Route::get('editar.vereador/{id}', 'EstoqueController@EstoqueEditarVereador')->name('editar.vereador');
Route::post('salvar.vereador.editado/{id}', 'EstoqueController@EstoqueSalvarVereadorEditado')->name('salvar.vereador.editado');
Route::delete('vereador.deletar/{id}', 'EstoqueController@EstoqueDeletaVereador')->name('vereador.deletar');


/*Entregas */
Route::get('pagina.lista.entregas', 'EstoqueController@EstoquePaginaListaEntrega')->name('pagina.lista.entregas');
Route::get('form.entrega', 'EstoqueController@EstoqueCadastraEntrega')->name('form.entrega');
Route::post('salvar.entrega', 'EstoqueController@EstoqueSalvarEntrega')->name('salvar.entrega');
Route::get('ajax/pegar-quantidade-produtos','EstoqueController@getQuantidadeProdutoAjax');



Route::get('form.compra', 'EstoqueController@EstoqueCadastraCompra')->name('form.compra');
Route::get('pagina.lista.compras', 'EstoqueController@EstoquePaginaListaCompras')->name('pagina.lista.compras');

Route::post('salvar.compra', 'EstoqueController@EstoqueSalvarCompra')->name('salvar.compra');

Route::delete('compra.deletar/{id}', 'EstoqueController@EstoqueDeletaCompra')->name('compra.deletar');

Route::delete('entrega.deletar/{id}', 'EstoqueController@EstoqueDeletaEntrega')->name('entrega.deletar');

Route::post('pesquisa.vereador', 'EstoqueController@EstoquePesquisaVereador')->name('pesquisa.vereador');

Route::post('pesquisa.produto', 'EstoqueController@EstoquePesquisaProduto')->name('pesquisa.produto');

Route::post('pesquisa.entrega', 'EstoqueController@EstoquePesquisaEntrega')->name('pesquisa.entrega');

Route::post('pesquisa.compra', 'EstoqueController@EstoquePesquisaCompra')->name('pesquisa.compra');


Route::get('pdf.lista.vereadores', 'EstoqueController@Html2PdfListaVereadores')->name('pdf.lista.vereadores');
Route::get('pdf.lista.entregas', 'EstoqueController@Html2PdfListaEntregas')->name('pdf.lista.entregas');

Route::get('pagina.lista.entregas.totais', 'EstoqueController@EstoquePaginaListaEntregasTotais')->name('pagina.lista.entregas.totais');

Route::post('pesquisa.entrega.total', 'EstoqueController@EstoquePesquisaEntregaTotal')->name('pesquisa.entrega.total');

Route::get('pdf.lista.produtos', 'EstoqueController@Html2PdfListaProdutos')->name('pdf.lista.produtos');

Route::get('pdf.lista.entregas.totais', 'EstoqueController@Html2PdfListaEntregasTotais')->name('pdf.lista.entregas.totais');

Route::get('pdf.lista.compras', 'EstoqueController@Html2PdfListaCompras')->name('pdf.lista.compras');


