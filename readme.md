# Controle de Estoque - CV Leão

## Descrição

Sistema de Controle de Estoque desenvolvido para a Câmara de Vereadores do Capão do Leão. Tem como principal objetivo o controle de todos os produtos solicitados pelos vereadores, funcionários da câmara e acessores bem como o controle das notas fiscais.

## Funcionalidades

  - Controle de Usuários
  - Controle de Produtos
  - Controle de Entregas
  - Controle de Compras

## Ferramentas Utilizadas

  - Sublime Text
  - Xampp
  - Composer

## Tecnologias Utilizadas

  - MySql
  - PHP
  - HTML
  - CSS
  - Bootstrap
  - Laravel

## Instalação

1 - Faça uma clonagem do projeto utilizando a ferramentas GitBash.

    git clone https://gitlab.com/renatosilva94/cvleao-controle-estoque.git

    
2 - Após ter instalado o Xampp, Composer e os devidos serviços estarem ativos e uma base de dados com o nome de cvleao criada, navegue até a pasta do projeto pelo prompt de comando e execute o comando de criação de tabelas.

    php artisan migrate
    
3 - Após execute o comando para iniciar a aplicação.

    php artisan serve
    
4 - Acesse o seguinte endereço através de um navegador da sua escolha.

    localhost:8000/lista.entrega
    
