<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
         Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_empresa');
            $table->string('numero_nf');
            $table->integer('quantidade_embalagens');
            $table->double('preco_embalagem');
            $table->integer('quantidade_produtos_embalagem');
            $table->double('preco_unitario');
            $table->double('preco_total');
            $table->string('data_entrada');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
