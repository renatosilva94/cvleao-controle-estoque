<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_produto', 'quantidade', 'data_compra');
    

}
