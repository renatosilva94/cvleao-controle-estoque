<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vereadores extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_vereador', 'cpf');
    

}
