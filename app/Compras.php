<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_empresa','numero_nf', 'quantidade_embalagens', 'preco_embalagem', 'quantidade_produtos_embalagem', 'preco_unitario', 'preco_total', 'produto_id', 'data_entrada');
    
      public function produto(){
        return $this->belongsTo('App\Produtos');
    }

}
