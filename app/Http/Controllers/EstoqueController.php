<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use App\Vereadores;
use App\Entregas;
use App\Compras;
use Illuminate\Support\Facades\DB;
use \PDF;
use \App;
use \View;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;



class EstoqueController extends Controller {


    
    public function EstoquePaginaInicialSistema() {

        $contaEntregas = DB::table('entregas')->count();
        $contaCompras = DB::table('compras')->count();
        $contaProdutos = DB::table('produtos')->count();
        $contaGastos = DB::table('compras')->sum('preco_total');

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();

        return view('pagina_inicial_sistema', compact('nomeProdutosIgual0','contaEntregas', 'contaCompras', 'contaProdutos', 'contaGastos', 'contaProdutosIgual0'));
    }

    public function EstoquePaginaListaProduto() {

        $produtos = Produtos::paginate(10);

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


        return view('pagina_lista_produtos', compact('produtos', 'contaProdutosIgual0', 'nomeProdutosIgual0'));
    }


   



    public function EstoqueSalvarProduto(Request $request) {
        $produtos = Produtos::create([
                    'nome_produto' => $request['nome_produto'],
                    'quantidade' => $request['quantidade'],
                    'data_compra' => $request['data_compra']
        ]);

        if ($produtos) {

            if (Input::file('imagem_produto')) {

                $ultimoId = $produtos->id;
                $imagemProdutoId = $ultimoId . '.png';
                $request->imagem_produto->move(public_path('imagens_produtos/'), $imagemProdutoId);
            } else {
                
            }

            return redirect()->route('pagina.lista.produtos');
        }
    }

    


    public function EstoqueSalvarProdutoEditado(Request $request, $id) {

        $this->validate($request, [
            'nome_produto' => 'required',
            'quantidade' => 'required',
            'data_compra' => 'required'
        ]);

        $dados = $request->all();

        $produtoId = Produtos::find($id);

        $alt = $produtoId->update($dados);

        if ($alt) {

            if (Input::file('imagem_produto')) {

                $ultimoId = $id;
                $imagemProdutoId = $ultimoId . '.png';
                $request->imagem_produto->move(public_path('imagens_produtos/'), $imagemProdutoId);
            } else {
                
            }

            return redirect()->route('pagina.lista.produtos');
        }
    }


    public function EstoquePaginaListaVereadores() {

        $vereadores = Vereadores::paginate(10);

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


        return view('pagina_lista_vereadores', compact('vereadores', 'contaProdutosIgual0', 'nomeProdutosIgual0'));
    }

    
   
    

    public function EstoqueSalvarVereador(Request $request) {
        $vereadores = Vereadores::create([
                    'nome_vereador' => $request['nome_vereador'],
                    'cpf' => $request['cpf']
        ]);

        if ($vereadores) {
            return redirect()->route('pagina.lista.vereadores');
        }
    }

    
    

    public function EstoqueSalvarVereadorEditado(Request $request, $id) {

        $this->validate($request, [
            'nome_vereador' => 'required',
            'cpf' => 'required'
        ]);

        $dados = $request->all();

        $vereadorId = Vereadores::find($id);

        $alt = $vereadorId->update($dados);

        if ($alt) {

            if (Input::file('imagem_vereador')) {

                $ultimoId = $id;
                $imagemVereadorId = $ultimoId . '.png';
                $request->imagem_vereador->move(public_path('imagens_vereadores/'), $imagemVereadorId);
            } else {
                
            }

            return redirect()->route('pagina.lista.vereadores');
        }
    }

    public function EstoqueDeletaVereador($id) {
        $vereador = Vereadores::find($id);
        if ($vereador->delete()) {
            return redirect()->route('pagina.lista.vereadores');
        }
    }

    public function EstoqueDeletaProduto($id) {
        $produto = Produtos::find($id);
        if ($produto->delete()) {
            return redirect()->route('pagina.lista.produtos');
        }
    }

    public function EstoquePaginaListaEntrega() {

        $entregas = Entregas::paginate(10);
                $vereadores = Vereadores::orderBy('nome_vereador')->get();
                        $produtos = Produtos::orderBy('nome_produto')->get();

                        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
                        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();
                


        return view('pagina_lista_entregas', compact('entregas', 'vereadores', 'produtos', 'contaProdutosIgual0', 'nomeProdutosIgual0'));
    }


    

    public function EstoqueSalvarEntrega(Request $request) {
        $entregas = Entregas::create([
                    'data_distribuicao' => $request['data_distribuicao'],
                    'destino' => $request['destino'],
                    'vereador_id' => $request['vereador_id'],
                    'produto_id' => $request['produto_id'],
                    'quantidade' => $request['quantidade']
        ]);


        if ($entregas) {


           $baixaEstoque = DB::table('produtos')
            ->where('id', $request['produto_id'])
            ->update(['quantidade' => (DB::raw($request['quantidadeEstoque'] - $request['quantidade']))]);


            return redirect()->route('pagina.lista.entregas');
        }
    }
    
    
    public function getQuantidadeProdutoAjax(Request $request)
    {
        $quantidadeProdutosAjax = DB::table("produtos")
                    ->where("id",$request->id)
                    ->pluck("quantidade","id");
        return response()->json($quantidadeProdutosAjax);
    }



    

    public function EstoquePaginaListaCompras() {

        $compras = Compras::paginate(10);
        $produtos = Produtos::orderBy('nome_produto')->get();

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


        return view('pagina_lista_compras', compact('compras', 'produtos', 'contaProdutosIgual0', 'nomeProdutosIgual0'));
    }
    

    public function EstoqueSalvarCompra(Request $request) {
        $compras = Compras::create([
                    'numero_nf' => $request['numero_nf'],
                    'quantidade_embalagens' => $request['quantidade_embalagens'],
                    'preco_embalagem' => $request['preco_embalagem'],
                    'quantidade_produtos_embalagem' => $request['quantidade_produtos_embalagem'],
                    'preco_unitario' => $request['preco_unitario'],
                    'preco_total' => $request['preco_total'],
                    'produto_id' => $request['produto_id'],
                    'data_entrada' => $request['data_entrada'],
                    'nome_empresa' => $request['nome_empresa'],
                    


        ]);

        if ($compras) {

             $abasteceEstoque = DB::table('produtos')
            ->where('id', $request['produto_id'])
            ->update(['quantidade' => (DB::raw($request['quantidadeEstoque'] + $request['quantidade_produtos_embalagem']))]);


            return redirect()->route('pagina.lista.compras');
        }
    }

    public function EstoqueDeletaCompra($id) {
        $compra = Compras::find($id);
        if ($compra->delete()) {
            return redirect()->route('pagina.lista.compras');
        }
    }


     public function EstoqueDeletaEntrega($id) {
        $entrega = Entregas::find($id);
        if ($entrega->delete()) {
            return redirect()->route('pagina.lista.entregas');
        }
    }

    public function EstoquePesquisaVereador(Request $request){

        $nome_vereador = $request->nome_vereador;
        $cpf = $request->cpf;

        $filtro = array();



         if (!empty($nome_vereador)) {
            array_push($filtro, array('nome_vereador', 'like', '%' . $nome_vereador . '%'));
        }

        if (!empty($cpf)) {
            array_push($filtro, array('cpf', 'like', '%' . $cpf . '%'));
        }

        $vereadores = Vereadores::where($filtro)->orderBy('nome_vereador')->paginate(100);

        return view('pagina_lista_vereadores', compact('vereadores'));



    }




public function EstoquePesquisaProduto(Request $request){

        $nome_produto = $request->nome_produto;
        $quantidade = $request->quantidade;



        $filtro = array();



         if (!empty($nome_produto)) {
            array_push($filtro, array('nome_produto', 'like', '%' . $nome_produto . '%'));
        }

        if (!empty($quantidade)) {
            array_push($filtro, array('quantidade', 'like', '%' . $quantidade . '%'));
        }

        $produtos = Produtos::where($filtro)->orderBy('nome_produto')->paginate(100);

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


        return view('pagina_lista_produtos', compact('produtos', 'contaProdutosIgual0', 'nomeProdutosIgual0'));



    }


    public function EstoquePesquisaEntrega(Request $request){

        $destino = $request->destino;
        $vereador_id = $request->vereador_id;
        $produto_id = $request->produto_id;

        $vereadores = Vereadores::orderBy('nome_vereador')->get();
        $produtos = Produtos::orderBy('nome_produto')->get();




        $filtro = array();

         if (!empty($destino)) {
            array_push($filtro, array('destino', 'like', '%' . $destino . '%'));
        }

        if (!empty($vereador_id)) {
            array_push($filtro, array('vereador_id', 'like', '%' . $vereador_id . '%'));
        }

         if (!empty($produto_id)) {
            array_push($filtro, array('produto_id', 'like', '%' . $produto_id . '%'));
        }

        $entregas = Entregas::where($filtro)->orderBy('id')->paginate(200);

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


        return view('pagina_lista_entregas', compact('entregas', 'vereadores', 'produtos', 'contaProdutosIgual0', 'nomeProdutosIgual0'));



    }



    public function EstoquePesquisaCompra(Request $request){

        $numero_nf = $request->numero_nf;

        $filtro = array();

         if (!empty($numero_nf)) {
            array_push($filtro, array('numero_nf', 'like', '%' . $numero_nf . '%'));
        }

       

        $compras = Compras::where($filtro)->orderBy('id')->paginate(200);

        $contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
        $nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


        return view('pagina_lista_compras', compact('compras', 'contaProdutosIgual0', 'nomeProdutosIgual0'));



    }





    public function Html2PdfListaVereadores(){


$vereadores = Vereadores::all();
 
    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('lista_vereador_pdf', compact('vereadores'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}


 public function Html2PdfListaEntregas(){


$entregas = Entregas::all();
 
    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('lista_entrega_pdf', compact('entregas'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}

public function Html2PdfListaProdutos(){


$produtos = Produtos::all();
 
    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('lista_produto_pdf', compact('produtos'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}



public function Html2PdfListaEntregasTotais(){


$entregas = Entregas::groupBy('produto_id')->groupBy('vereador_id')->get();
 
    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('lista_entrega_total_pdf', compact('entregas'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}


public function Html2PdfListaCompras(){


$compras = Compras::all();
 
    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('lista_compra_pdf', compact('compras'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}

 
 public function EstoquePaginaListaEntregasTotais(Request $request) {

             $entregas = Entregas::groupBy('produto_id')->groupBy('vereador_id')->get();
             $vereadores = Vereadores::orderBy('nome_vereador')->get();
             $produtos = Produtos::orderBy('nome_produto')->get();

          $items = collect($entregas);
$page = Input::get('page', 1);
$perPage = 10;

$contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
$nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();


$pagination = new LengthAwarePaginator(
    $items->forPage($page, $perPage), $items->count(), $perPage, $page);
        return view('pagina_lista_entregas_totais', compact('contaProdutosIgual0','nomeProdutosIgual0','entregas', 'vereadores', 'produtos', 'pagination', 'page'
            , 'perPage', 'items'));
    }




public function EstoquePesquisaEntregaTotal(Request $request){

        $vereador_id = $request->vereador_id;
        $produto_id = $request->produto_id;

        $vereadores = Vereadores::orderBy('nome_vereador')->get();
        $produtos = Produtos::orderBy('nome_produto')->get();

        $filtro = array();

        if (!empty($vereador_id)) {
            array_push($filtro, array('vereador_id', 'like', '%' . $vereador_id . '%'));
        }

        if (!empty($produto_id)) {
            array_push($filtro, array('produto_id', 'like', '%' . $produto_id . '%'));
        }

        $entregas = Entregas::where($filtro)->groupBy('produto_id')->groupBy('vereador_id')
        ->get();
/*
        $items_per_page = 10;
        $pagination = new LengthAwarePaginator(
        $entregas->forPage($request->has('lista_entrega_total') ?: 1, $items_per_page),
        $entregas->count(),
        $items_per_page

        );
        */
          $items = collect($entregas);
$page = Input::get('page', 1);
$perPage = 10;

$pagination = new LengthAwarePaginator(
    $items->forPage($page, $perPage), $items->count(), $perPage, $page
);

$contaProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->count();
$nomeProdutosIgual0 = DB::table('produtos')->where('quantidade', 0)->get();





        return view('pagina_lista_entregas_totais', compact('contaProdutosIgual0','nomeProdutosIgual0','entregas', 'vereadores', 'produtos', 'pagination', 'page'
            , 'perPage', 'items'));



    }
    



}
