<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entregas extends Model {

    public $timestamps = false;
    protected $fillable = array('data_distribuicao', 'destino', 'vereador_id', 'produto_id', 'quantidade');
    
    public function vereador(){
        return $this->belongsTo('App\Vereadores');
    }
    
     public function produto(){
        return $this->belongsTo('App\Produtos');
    }

}
