<div class='col-sm-11'>
    <h2> Produtos </h2>
</div>

<div class='col-sm-12'>

    <table class="table table-hover" cellpadding="10">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome do Produto</th>
                <th>Quantidade</th>
                <th>Data de Compra</th>

            </tr>
        </thead>
        <tbody>


            @foreach($produtos as $produto)


             <tr>
                <td style="text-align: center">{{$produto->id}}</td>
                <td>{{$produto->nome_produto}}</td>
                <td>{{$produto->quantidade}}</td>
                <td>{{date('d-m-Y', strtotime($produto->data_compra))}}</td>
                
            @endforeach            
            </tr>


        </tbody>
    </table>    


</div>


