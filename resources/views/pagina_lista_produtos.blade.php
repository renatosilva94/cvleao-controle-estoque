@extends('principal')

@section('conteudo')

<div class="modal fade" id="modalNovoProduto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Produto</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.produto')}}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
            <label for="nome_produto">Nome do Produto:</label>
            <input type="text" class="form-control" id="nome_produto"
                   name="nome_produto"
                   required>
        </div>

        
                
        <div class="form-group">
            <label for="quantidade">Quantidade:</label>
            <input type="number" class="form-control" id="quantidade"
                   name="quantidade" 
                   required>
        </div>
        

        <div class="form-group">
            <label for="data_compra">Data de Compra:</label>
            <input type="date" class="form-control" id="data_compra"
                   name="data_compra" 
                   >
        </div>


                    <div class="form-group">
                        <label for="imagem_produto"> Imagem do Produto: </label>
                        <input type="file" id="imagem_produto" name="imagem_produto"
                               onchange="previewFile()"
                               class="form-control">
                    </div>


                    <div class="col-sm-6">

                        {!!"<img src='imagens_produtos/sem_foto.png' id='imagem_produto_preview' height='150px' width='150px' alt='Foto do Produto' class='img-circle'>"!!}

                    </div>

                    <script>
                        function previewFile() {
                            var preview = document.getElementById('imagem_produto_preview');
                            var file = document.getElementById('imagem_produto').files[0];
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                preview.src = reader.result;
                            };

                            if (file) {
                                reader.readAsDataURL(file);
                            } else {
                                preview.src = "";
                            }
                        }

                    </script>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Produto</button>

                </form>

            </div>
        </div>
    </div>
</div>








        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


                <div class='col-sm-3'>
    <h2> Produtos </h2>
</div>



<form method="post" action="{{route('pesquisa.produto')}}">
    {{ csrf_field() }}

    
<div class="row">
    <div class='col-sm-6'>
        <div class="form-group">
            <label for="nome_produto">Nome do Produto:</label>
            <input type="text" class="form-control" id="nome_produto"
                   name="nome_produto">
        </div>
    </div>

    <div class='col-sm-6'>
        <div class="form-group">
            <label for="quantidade">Quantidade:</label>
            <input type="number" class="form-control" id="quantidade"
                   name="quantidade">
        </div>
    </div>
                    </div>
    <div class='col-sm-12'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>

        <button type="button" class="btn btn-success" data-toggle="modal" 
        data-target="#modalNovoProduto">Novo Produto </button>


                <a href="{{route('pdf.lista.produtos')}}" class="btn btn-info">Gerar Pdf</a>

                <a href="{{route('pagina.lista.produtos')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
    </div>    

</form>

<br>
<br>

                    <div class="container-fluid">

                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-wrapper table--no-card m-b-30">

                                @if (count($produtos)==0)
    <div class="alert alert-danger">
        Não há produtos com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                <th>Código</th>
                <th>Nome do Produto</th>
                <th>Quantidade</th>
                <th>Data de Compra</th>


                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($produtos as $produto)


                                         <!-- Modal Editar Produto -->

                                         <style>
                                             
                                         .modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    

}</style>

                    <div class="modal fade" id="modalEditarProduto{{$produto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel"><b>Editar Produto</b></h3>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('salvar.produto.editado', $produto->id)}}" enctype="multipart/form-data">

                                    {{ csrf_field() }}

<div class="form-group">
    <label for="nome_produto">Nome do Produto:</label>
    <input type="text" class="form-control" id="nome_produto"
           name="nome_produto" 
           value="{{$produto->nome_produto or old('nome_produto')}}"
           required>
</div>


        
<div class="form-group">
    <label for="quantidade">Quantidade:</label>
    <input type="number" class="form-control" id="quantidade"
           name="quantidade" 
           value="{{$produto->quantidade or old('quantidade')}}"
           required>
</div>


<div class="form-group">
    <label for="data_compra">Data de Compra:</label>
    <input type="date" class="form-control" id="data_compra"
           name="data_compra" 
           value="{{$produto->data_compra or old('data_compra')}}"
           >
</div>


<div class="form-group">
                                            <label for="imagem_produto"> Imagem do Produto: </label>
                                            <input type="file" id="imagem_produto_editar" name="imagem_produto"
                                                   class="form-control">
                                                   <!-- onChange="previewFileEditar()" -->
                                        </div>


                                        <div class="col-sm-6">

                                            @php
                                            if(file_exists(public_path('imagens_produtos/'.$produto->id.'.png'))){
                                            $imagem_produto = '../imagens_produtos/'.$produto->id.'.png';
                                            } else {
                                            $imagem_produto = '../imagens_produtos/sem_foto.png';
                                            }
                                            @endphp

                                            {!!"<img src=$imagem_produto id='imagem_produto_preview_editar' height='150px' width='150px' alt='Foto do Produto' class='img-cirle'>"!!}

                                        </div>



                                        <script>
                                            window.onload = () => {

                                                $('input[type="file"]').on('change', e => {
                                                    const activeModal = $('.modal.fade.in');
                                                    const preview = activeModal.find('#imagem_produto_preview_editar');
                                                    const file = e.target.files[0];
                                                    const reader = new FileReader();
                                                    
                                                    reader.onload = function () {
                                                        preview.attr('src', reader.result);
                                                    };
                                                    
                                                    if (file) {
                                                        reader.readAsDataURL(file);
                                                    } else {
                                                        preview.src = "";
                                                    }
                                                });
                                                
                                               
                                            }

                                        </script>


<button type="submit" class="btn btn-primary">Salvar Produto</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    

                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Fim da Modal Editar Produtos -->
























            <tr>
                <td style="text-align: center">{{$produto->id}}</td>
                <td>{{$produto->nome_produto}}</td>
                <td>{{$produto->quantidade}}</td>
                <td>{{date('d-m-Y', strtotime($produto->data_compra))}}</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#modalEditarProduto{{$produto->id}}"
                       class="btn btn-warning btn-sm" 
                       role="button">Editar</a> &nbsp;&nbsp;


                       
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('produto.deletar', $produto->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Produto?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>
                                            
                                        
                                        </tbody>
                                    </table>

                                    <br>

                                    <h4> {{ $produtos->links() }} </h4>

                                </div>
                        </div>
                </div>
            </div>
                                
@endsection