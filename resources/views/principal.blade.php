<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Controle de Estoque - Câmara de Vereadores de Capão do Leão</title>

<!-- JQUERY --> 
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

 <script>

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

</script>


</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="{{route('pagina.inicial.sistema')}}">
                            <img src="imagens/cvleao.png" width="50px" height="50px" /> <b style="color: #000">Estoque</b>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                       
                    <li>
                            <a href="{{route('pagina.inicial.sistema')}}">
                                <i class="fas fa-home"></i>Página Inicial</a>
                        </li>
                    
                        <li>
                        <a href="{{route('pagina.lista.produtos')}}"> 
                                 <i class="fas fa-archive"></i>Produtos</a>
                        </li>


                        <li>
                        <a href="{{route('pagina.lista.vereadores')}}">
                                <i class="fas fa-user"></i>Vereadores</a>
                        </li>
                        <li>
                        <a href="{{route('pagina.lista.entregas')}}">
                                <i class="far fa-check-square"></i>Entregas</a>
                        </li>
                        <li>
                        <a href="{{route('pagina.lista.compras')}}">
                                <i class="fas fa-usd"></i>Compras</a>
                        </li>
                        <li>
                        <a href="{{route('pagina.lista.entregas.totais')}}">
                                <i class="fas fa-arrows-alt"></i>Entregas Totais</a>
                        </li>
                        
                        
                        
                        
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="{{route('pagina.inicial.sistema')}}">
                <img src="imagens/cvleao.png" width="60px" height="60px" alt="" /> <b style="color: #000">Estoque</b>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                     
                    <li>
                            <a href="{{route('pagina.inicial.sistema')}}">
                                <i class="fas fa-home"></i>Página Inicial</a>
                        </li>
                        <li>
                            <a href="{{route('pagina.lista.produtos')}}">
                                <i class="fas fa-archive"></i>Produtos</a>
                        </li>
                        <li>
                            <a href="{{route('pagina.lista.vereadores')}}">
                                <i class="fas fa-user"></i>Vereadores</a>
                        </li>
                        <li>
                            <a href="{{route('pagina.lista.entregas')}}">
                                <i class="far fa-check-square"></i>Entregas</a>
                        </li>
                        <li>
                        <a href="{{route('pagina.lista.compras')}}">
                                <i class="fas fa-usd"></i>Compras</a>
                        </li>
                        <li>
                        <a href="{{route('pagina.lista.entregas.totais')}}">
                                <i class="fas fa-arrows-alt"></i>Entregas Totais</a>
                        </li>
                        
                        
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                       

                            <div class="header-button">
                                <div class="noti-wrap">
                                 <div class="noti__item js-item-menu">
                                      
                                    
                                   
                                    
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">{{$contaProdutosIgual0}}</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>Notificações</p>
                                            </div>
                                            
                                            
                                            
                                            @foreach($nomeProdutosIgual0 as $produtosEstoqueBaixo)

                                            <div class="notifi__item">

                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>


                                                <div class="content">
                                                    <p>O produto <b>{{$produtosEstoqueBaixo->nome_produto}}</b> está em falta.</p>

                                                </div>



                                            </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            @yield('conteudo')
            
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
