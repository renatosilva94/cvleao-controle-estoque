@extends('principal')

@section('conteudo')

        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


                <div class='col-sm-11'>
    <h2> Entregas Totais</h2>
</div>


<form method="post" action="{{route('pesquisa.entrega.total')}}">
    {{ csrf_field() }}


    <div class='col-sm-4'>
    <div class="form-group">
            <label for="vereador_id">Usuário:</label>
            <select class="form-control" id="vereador_id" name="vereador_id">
            <option></option>
            @foreach($vereadores as $vereador)
            <option value="{{$vereador->id}}" name="vereador_id">{{$vereador->nome_vereador}}</option>
            @endforeach
            </select>
        </div>
    </div>

    <div class='col-sm-4'>
    <div class="form-group">
            <label for="produto_id">Produto:</label>
            <select class="form-control" id="produto_id" name="produto_id">
            <option></option>
            @foreach($produtos as $produto)
            <option value="{{$produto->id}}" name="produto_id">{{$produto->nome_produto}}</option>
            @endforeach
            </select>
        </div>
    </div>


    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
                <a href="{{route('pdf.lista.entregas.totais')}}" class="btn btn-info">Gerar Pdf</a>
                <a href="{{route('pagina.lista.entregas.totais')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>

    </div>    

</form>

<br>
<br>


<div class='col-sm-12'>

    @if (count($entregas)==0)
    <div class="alert alert-danger">
        Não há entregas com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                                        <th>Usuário</th>
                <th>Nome do Produto</th>
                <th>Quantidade Total Entregue</th>
                

            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($entregas->sortBy('sum(quantidade)') as $entrega)
            <tr>
                <td>{{$entrega->vereador->nome_vereador}}</td>
                <td>{{$entrega->produto->nome_produto}}</td>
                
        <?php  $total_produtos_entregue = $entrega->where('vereador_id', '=', $entrega->vereador->id)->where('produto_id', '=', $entrega->produto->id)
        ->sum('quantidade'); ?>



                <td>{{$total_produtos_entregue}}</td>
                
                
                
            @endforeach
            </tr>
            


        </tbody>
    </table>    

     

                                </div>
                        </div>
                </div>
            </div>
                                
@endsection