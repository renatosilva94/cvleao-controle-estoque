<div class='col-sm-11'>
    <h2> Entregas Totais </h2>
</div>

<div class='col-sm-12'>

    <table class="table table-hover" cellpadding="10">
        <thead>
            <tr>
             <th>Usuário</th>
                <th>Nome do Produto</th>
                <th>Quantidade Total Entregue</th>

            </tr>
        </thead>
        <tbody>


            @foreach($entregas->sortBy('sum(quantidade)') as $entrega)
            <tr>
                <td>{{$entrega->vereador->nome_vereador}}</td>
                <td>{{$entrega->produto->nome_produto}}</td>
                
        <?php  $total_produtos_entregue = $entrega->where('vereador_id', '=', $entrega->vereador->id)->where('produto_id', '=', $entrega->produto->id)
        ->sum('quantidade'); ?>



                <td>{{$total_produtos_entregue}}</td>
                
                
                
            @endforeach       
            </tr>


        </tbody>
    </table>    


</div>


