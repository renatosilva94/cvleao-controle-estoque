<div class='col-sm-11'>
    <h2> Vereadores </h2>
</div>

<div class='col-sm-12'>

    <table class="table table-hover" cellpadding="10">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome do Vereador</th>
                <th>CPF</th>


            </tr>
        </thead>
        <tbody>


            @foreach($vereadores as $vereador)


             <tr>
                <td style="text-align: center">{{$vereador->id}}</td>
                <td>{{$vereador->nome_vereador}}</td>
                <td>{{$vereador->cpf}}</td>
            @endforeach            
            </tr>


        </tbody>
    </table>    


</div>


