<div class='col-sm-11'>
    <h2> Compras </h2>
</div>

<div class='col-sm-12'>

    <table class="table table-hover" cellpadding="10">
        <thead>
            <tr>
                 <th>Código</th>
                 <th>Data de Entrada</th>
                <th>Numero da Nota Fiscal</th>
                <th>Quantidade de Embalagens</th>
                <th>Quantidade de Produtos na Embalagem</th>
                <th>Preço da Embalagem</th>
                <th>Preço Unitário</th>
                <th>Preço Total</th>
                <th>Produto</th>

            </tr>
        </thead>
        <tbody>


            @foreach($compras as $compra)
            <tr>
                <td style="text-align: center">{{$compra->id}}</td>
                <td>{{$compra->data_entrada}}</td>
                <td>{{$compra->numero_nf}}</td>
                <td>{{$compra->quantidade_embalagens}}</td>
                <td>{{$compra->quantidade_produtos_embalagem}}</td>
                <td>{{number_format($compra->preco_embalagem, 2)}}</td>
                <td>{{number_format($compra->preco_unitario, 2)}}</td>
                <td>{{number_format($compra->preco_total, 2)}}</td>
                <td>{{$compra->produto->nome_produto}}</td>

               
                @endforeach        
            </tr>


        </tbody>
    </table>    


</div>


