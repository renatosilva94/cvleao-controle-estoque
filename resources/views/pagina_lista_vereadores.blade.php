@extends('principal')

@section('conteudo')


<div class="modal fade" id="modalNovoVereador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Vereador</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.vereador')}}" enctype="multipart/form-data">

                {{ csrf_field() }}

<div class="form-group">
    <label for="nome_vereador">Nome do Vereador:</label>
    <input type="text" class="form-control" id="nome_vereador"
           name="nome_vereador" 
           required>
</div>


        
<div class="form-group">
    <label for="cpf">CPF:</label>
    <input type="text" class="form-control" id="cpf"
           name="cpf" 
           required>
</div>
    


                    <div class="form-group">
                        <label for="imagem_vereador"> Imagem do Vereador: </label>
                        <input type="file" id="imagem_vereador" name="imagem_vereador"
                               onchange="previewFile()"
                               class="form-control">
                    </div>


                    <div class="col-sm-6">

                        {!!"<img src='imagens_vereadores/sem_foto.png' id='imagem_vereador_preview' height='150px' width='150px' alt='Foto do Vereador' class='img-circle'>"!!}

                    </div>

                    <script>
                        function previewFile() {
                            var preview = document.getElementById('imagem_vereador_preview');
                            var file = document.getElementById('imagem_vereador').files[0];
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                preview.src = reader.result;
                            };

                            if (file) {
                                reader.readAsDataURL(file);
                            } else {
                                preview.src = "";
                            }
                        }

                    </script>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Vereador</button>

                </form>

            </div>
        </div>
    </div>
</div>
        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


                <div class='col-sm-11'>
    <h2> Vereadores </h2>
</div>

<form method="post" action="{{route('pesquisa.vereador')}}">
    {{ csrf_field() }}

    <div class="row">

    <div class='col-sm-5'>
        <div class="form-group">
            <label for="nome_vereador">Nome do Vereador:</label>
            <input type="text" class="form-control" id="nome_vereador"
                   name="nome_vereador">
        </div>
    </div>

    <div class='col-sm-5'>
        <div class="form-group">
            <label for="cpf">CPF:</label>
            <input type="text" class="form-control" id="cpf"
                   name="cpf">
        </div>
    </div>

                    </div>

    <div class='col-sm-8'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="#" data-toggle="modal" data-target="#modalNovoVereador" class="btn btn-success">Cadastrar Vereador</a>
        <a href="{{route('pdf.lista.vereadores')}}" class="btn btn-info">Gerar Pdf</a>
        <a href="{{route('pagina.lista.vereadores')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
        
    </div>    

</form>

<br>
<br>

<div class='col-sm-12'>

    @if (count($vereadores)==0)

    <div class="alert alert-danger">
        Não há vereadores com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                                        <th>Código</th>
                <th>Nome do Vereador</th>
                <th>CPF</th>


                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($vereadores as $vereador)




<!-- Modal Editar Vereador -->

<style>
                                             
                                             .modal-backdrop {
        /* bug fix - no overlay */    
        display: none;    
    
    }</style>
    
                        <div class="modal fade" id="modalEditarVereador{{$vereador->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel"><b>Editar Vereador</b></h3>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="{{route('salvar.vereador.editado', $vereador->id)}}" enctype="multipart/form-data">
    
                                        {{ csrf_field() }}

<div class="form-group">
    <label for="nome_vereador">Nome do Vereador:</label>
    <input type="text" class="form-control" id="nome_vereador"
           name="nome_vereador" 
           value="{{$vereador->nome_vereador or old('nome_vereador')}}"
           required>
</div>


        
<div class="form-group">
    <label for="cpf">CPF:</label>
    <input type="text" class="form-control" id="cpf"
           name="cpf" 
           value="{{$vereador->cpf or old('cpf')}}"
           required>
</div>
    
    
    <div class="form-group">
                                                <label for="imagem_vereador"> Imagem do Vereador: </label>
                                                <input type="file" id="imagem_vereador_editar" name="imagem_vereador"
                                                       class="form-control">
                                                       <!-- onChange="previewFileEditar()" -->
                                            </div>
    
    
                                            <div class="col-sm-6">
    
                                                @php
                                                if(file_exists(public_path('imagens_vereadores/'.$vereador->id.'.png'))){
                                                $imagem_vereador = '../imagens_vereadores/'.$vereador->id.'.png';
                                                } else {
                                                $imagem_vereador = '../imagens_vereadores/sem_foto.png';
                                                }
                                                @endphp
    
                                                {!!"<img src=$imagem_vereador id='imagem_vereador_preview_editar' height='150px' width='150px' alt='Foto do Vereador' class='img-cirle'>"!!}
    
                                            </div>
    
    
    
                                            <script>
                                                window.onload = () => {
    
                                                    $('input[type="file"]').on('change', e => {
                                                        const activeModal = $('.modal.fade.in');
                                                        const preview = activeModal.find('#imagem_vereador_preview_editar');
                                                        const file = e.target.files[0];
                                                        const reader = new FileReader();
                                                        
                                                        reader.onload = function () {
                                                            preview.attr('src', reader.result);
                                                        };
                                                        
                                                        if (file) {
                                                            reader.readAsDataURL(file);
                                                        } else {
                                                            preview.src = "";
                                                        }
                                                    });
                                                    
                                                   
                                                }
    
                                            </script>
    
    
    <button type="submit" class="btn btn-primary">Salvar Vereador</button>        
    <button type="reset" class="btn btn-warning">Limpar</button>        
    </form>    
    
                                    </div>
                                </div>
                            </div>
                        </div>
    
    
    
    
                        <!-- Fim da Modal Editar Vereador -->

























            <tr>
                <td style="text-align: center">{{$vereador->id}}</td>
                <td>{{$vereador->nome_vereador}}</td>
                <td>{{$vereador->cpf}}</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#modalEditarVereador{{$vereador->id}}"
                       class="btn btn-warning" 
                       role="button">Editar</a> &nbsp;&nbsp;
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('vereador.deletar', $vereador->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Vereador?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form>  &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
                                    </table>
                                    <br>

                                    <h4>                         {{ $vereadores->links() }} </h4>

                                </div>
                        </div>
                </div>
            </div>
                                
@endsection