@extends('principal')

@section('conteudo')

<!-- JAVASCRIPT DOS CALCULOS -->



<script>
    $(document).ready(function () {
        var quantidade_embalagens = document.getElementById('quantidade_embalagens');
        var preco_embalagem = document.getElementById('preco_embalagem');
        
       



        //Calculo do Preço Total
        $('#preco_embalagem').on('focusout', function () {
            var preco_total_resultado = (quantidade_embalagens.value) * parseFloat(preco_embalagem.value);
            preco_total.value = (parseFloat(preco_total_resultado).toFixed(2));
        });

        //Calculo de Preço Unitário

        $('#quantidade_produtos_embalagem').on('focusout', function () {
            var preco_unitario_resultado = (parseFloat(preco_embalagem.value) / (quantidade_produtos_embalagem.value)) ;
            preco_unitario.value = (parseFloat(preco_unitario_resultado).toFixed(2));
        });




        });
      

</script>



<!-- FIM DO JAVASCRIPT -->

<div class="modal fade" id="modalNovaCompra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Nova Compra</b></h3>
            </div>
            <div class="modal-body">
            <form method="post" action="{{route('salvar.compra')}}">

{{ csrf_field() }}

<div class="form-group">
    <label for="data_entrada">Data de Entrada:</label>
    <input type="text" class="form-control" id="data_entrada"
           name="data_entrada"
           required value='<?php echo (new \DateTime())->format('Y-m-d'); ?>'>
</div>

        <div class="form-group">
            <label for="nome_empresa">Nome da Empresa:</label>
            <input type="text" class="form-control" id="nome_empresa"
                   name="nome_empresa">
        </div>


<div class="form-group">
    <label for="numero_nf">Numero da Nota Fiscal:</label>
    <input type="text" class="form-control" id="numero_nf"
           name="numero_nf"
           required>
</div>

<div class="form-group">
    <label for="produto_id">Produto:</label>
    <select class="form-control" id="produto_id" name="produto_id">
    <option></option>
    @foreach($produtos as $produto)
    <option value="{{$produto->id}}" name="produto_id">{{$produto->nome_produto}}</option>
    @endforeach
    </select>
</div>

<div class="form-group">
    <label for="quantidade_embalagens">Quantidade de Embalagens:</label>
    <input type="number" class="form-control" id="quantidade_embalagens"
           name="quantidade_embalagens" value="quantidade_embalagens"
           required>
</div>


<div class="form-group">
    <label for="preco_embalagem">Preço da Embalagem:</label>
    <input type="number" class="form-control" id="preco_embalagem"
           name="preco_embalagem" value="preco_embalagem"
           required step="0.01">
</div>  


   <div class="form-group">
    <label for="quantidade_produtos_embalagem">Quantidade de Produtos na Embalagem (Informação que aumenta o estoque):</label>
    <input type="number" class="form-control" id="quantidade_produtos_embalagem"
           name="quantidade_produtos_embalagem" value="quantidade_produtos_embalagem"
           required>
</div> 



   <div class="form-group">
    <label for="preco_unitario">Preço Unitário:</label>
    <input type="number" class="form-control" id="preco_unitario"
           name="preco_unitario" value="preco_unitario"
           required step="0.01">
</div> 


   <div class="form-group">
    <label for="preco_total">Preço Total:</label>
    <input type="number" class="form-control" id="preco_total"
           name="preco_total" value="preco_total"
           required step="0.01">

<script>

$('#produto_id').on('change', function () {
                            var produtoID = $(this).val();
                            if (produtoID) {
                                $.ajax({
                                    type: "GET",
                                    url: "{{url('ajax/pegar-quantidade-produtos')}}?id=" + produtoID,
                                    success: function (res) {
                                        if(res) {
                                            
                                            $("#quantidadeEstoque").empty();
                                            $.each(res, function (key, value) {
                                              //  $("#quantidadeEstoque").append('<option value="' + key + '">' + value + '</option>');

                                                $("#quantidadeEstoque").val(value);
                                            });

                                        } else {

                                            $("#quantidadeEstoque").empty();

                                        }
                                    }
                                });
                            } else {
                                $("#quantidadeEstoque").empty();
                            }

                        });
                        

</script>

</div> 

    <div class="form-group">
    <label for="quantidadeEstoque">Quantidade do Produto em Estoque:</label>
    <input type="number" class="form-control" id="quantidadeEstoque"
           name="quantidadeEstoque"
           required readonly="readonly">
</div>



<button type="submit" class="btn btn-primary">Salvar</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    

            </div>
        </div>
    </div>
</div>
        

        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


                <div class='col-sm-11'>
    <h2> Compras </h2>
</div>

<form method="post" action="{{route('pesquisa.compra')}}">
    {{ csrf_field() }}

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="numero_nf">Numero da Nota Fiscal:</label>
            <input type="text" class="form-control" id="numero_nf"
                   name="numero_nf">
        </div>
    </div>

    

    <div class='col-sm-9'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalNovaCompra">Nova Compra</a>
                        <a href="{{route('pdf.lista.compras')}}" class="btn btn-info">Gerar Pdf</a>
                        <a href="{{route('pagina.lista.compras')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>

    </div>    

</form>

<br>
<br>

<div class='col-sm-12'>

    @if (count($compras)==0)
    <div class="alert alert-danger">
        Não há compras com o filtro informado...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                                        <th>Código</th>
                <th>Data de Entrada</th>
                <th>Numero da Nota Fiscal</th>
                <th>Quantidade de Embalagens</th>
                <th>Quantidade de Produtos na Embalagem</th>
                <th>Preço da Embalagem</th>
                <th>Preço Unitário</th>
                <th>Preço Total</th>
                <th>Produto</th>

                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($compras as $compra)
            <tr>
                <td style="text-align: center">{{$compra->id}}</td>
                <td>{{date('d-m-Y', strtotime($compra->data_entrada))}}</td>
                <td>{{$compra->numero_nf}}</td>
                <td>{{$compra->quantidade_embalagens}}</td>
                <td>{{$compra->quantidade_produtos_embalagem}}</td>
                <td>{{number_format($compra->preco_embalagem, 2)}}</td>
                <td>{{number_format($compra->preco_unitario, 2)}}</td>
                <td>{{number_format($compra->preco_total, 2)}}</td>
                <td>{{$compra->produto->nome_produto}}</td>


                <td>
                    
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('compra.deletar', $compra->id)}}"
                          onsubmit="return confirm('Confirma Exclusão da Compra?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
    </table>    
    <br>
                                    <h4>                                             {{ $compras->links() }}
 </h4>

                                </div>
                        </div>
                </div>
            </div>
                                
@endsection