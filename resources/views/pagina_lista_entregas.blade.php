@extends('principal')

@section('conteudo')

<div class="modal fade" id="modalNovaEntrega" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Nova Entrega</b></h3>
            </div>
            <div class="modal-body">
            <form method="post" action="{{route('salvar.entrega')}}">

{{ csrf_field() }}



<div class="form-group">
    <label for="data_distribuicao">Data de Distribuição:</label>
    <input type="text" class="form-control" id="data_distribuicao"
           name="data_distribuicao"
           required value='<?php echo (new \DateTime())->format('Y-m-d'); ?>'>
</div>



        
<div class="form-group">
    <label for="destino">Destino:</label>
    <input type="text" class="form-control" id="destino"
           name="destino" value="-"
           required>
</div>


<div class="form-group">
    <label for="vereador_id">Usuário:</label>
    <select class="form-control" id="vereador_id" name="vereador_id">
    <option></option>
    @foreach($vereadores as $vereador)
    <option value="{{$vereador->id}}" name="vereador_id">{{$vereador->nome_vereador}}</option>
    @endforeach
    </select>
</div>

<div class="form-group">
    <label for="produto_id">Produto:</label>
    <select class="form-control" id="produto_id" name="produto_id">
    <option></option>
    @foreach($produtos as $produto)
    <option value="{{$produto->id}}" name="produto_id">{{$produto->nome_produto}}</option>
    @endforeach
    </select>
</div>

<div class="form-group">
    <label for="quantidade">Quantidade:</label>
    <input type="number" min="0" class="form-control" id="quantidade"
           name="quantidade"
           required>
</div>


<div class="form-group">
    <label for="quantidadeEstoque">Quantidade do Produto em Estoque:</label>
    <input type="number" class="form-control" id="quantidadeEstoque"
           name="quantidadeEstoque"
           required readonly="readonly">
</div>


<script>

$('#produto_id').on('change', function () {
                            var produtoID = $(this).val();
                            if (produtoID) {
                                $.ajax({
                                    type: "GET",
                                    url: "{{url('ajax/pegar-quantidade-produtos')}}?id=" + produtoID,
                                    success: function (res) {
                                        if(res) {
                                            
                                            $("#quantidadeEstoque").empty();
                                            $.each(res, function (key, value) {
                                              //  $("#quantidadeEstoque").append('<option value="' + key + '">' + value + '</option>');

                                                $("#quantidadeEstoque").val(value);
                                            });

                                        } else {

                                            $("#quantidadeEstoque").empty();

                                        }
                                    }
                                });
                            } else {
                                $("#quantidadeEstoque").empty();
                            }

                        });
                        

</script>



<!-- FIM DO SCRIPT -->



<button type="submit" class="btn btn-primary">Salvar</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    
            </div>
        </div>
    </div>
</div>
        
        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


                <div class='col-sm-11'>
    <h2> Entregas </h2>
</div>


<form method="post" action="{{route('pesquisa.entrega')}}">
    {{ csrf_field() }}

    <div class="row">

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="destino">Destino:</label>
            <input type="text" class="form-control" id="destino"
                   name="destino">
        </div>
    </div>

    <div class='col-sm-4'>
    <div class="form-group">
            <label for="vereador_id">Usuário:</label>
            <select class="form-control" id="vereador_id" name="vereador_id">
            <option></option>
            @foreach($vereadores as $vereador)
            <option value="{{$vereador->id}}" name="vereador_id">{{$vereador->nome_vereador}}</option>
            @endforeach
            </select>
        </div>
    </div>

    <div class='col-sm-4'>
    <div class="form-group">
            <label for="produto_id">Produto:</label>
            <select class="form-control" id="produto_id" name="produto_id">
            <option></option>
            @foreach($produtos as $produto)
            <option value="{{$produto->id}}" name="produto_id">{{$produto->nome_produto}}</option>
            @endforeach
            </select>
        </div>
    </div>

                    </div>


    <div class='col-sm-9'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="#" data-toggle="modal" data-target="#modalNovaEntrega" class="btn btn-success">Cadastrar Entrega</a>
                <a href="{{route('pdf.lista.entregas')}}" class="btn btn-info">Gerar Pdf</a>
                <a href="{{route('pagina.lista.entregas')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
    </div>    

</form>

<br>
<br>

<div class='col-sm-12'>

    @if (count($entregas)==0)
    <div class="alert alert-danger">
        Não há entregas com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                                        <th>Código</th>
                <th>Data de Distribuição</th>
                <th>Destino</th>
                <th>Usuário</th>
                <th>Nome do Produto</th>
                <th>Quantidade</th>
                

                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($entregas as $entrega)
            <tr>
                <td style="text-align: center">{{$entrega->id}}</td>
                <td>{{date('d-m-Y', strtotime($entrega->data_distribuicao))}}</td>
                <td>{{$entrega->destino}}</td>
                <td>{{$entrega->vereador->nome_vereador}}</td>
                <td>{{$entrega->produto->nome_produto}}</td>
                <td>{{$entrega->quantidade}}</td>
                
                
                <td>

                    <form style="display: inline-block"
                          method="post"
                          action="{{route('entrega.deletar', $entrega->id)}}"
                          onsubmit="return confirm('Confirma Exclusão da Entrega?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
            @endforeach
            </tr>
            


        </tbody>
    </table>    
    <br>
                                    <h4>                                             {{ $entregas->links() }}
 </h4>

                                </div>
                        </div>
                </div>
            </div>
                                
@endsection